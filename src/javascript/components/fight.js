import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over

    const firstFighterIndicator = document.getElementById('left-fighter-indicator');
    const secondFighterIndicator = document.getElementById('right-fighter-indicator');

    const firstFighterInitHealth = firstFighter.health;
    const secondFighterInitHealth = secondFighter.health;

    const keypressHandler = (e) => {
      function criticalAttackClosure(callback) {
        let timeout = 0;
        if (timeout === 0) {
          return () => {
            timeout = 10;
            callback();
            setTimeout(() => {
              timeout = 0;
            }, 10000);
          };
        }
        return null;
      }

      switch (e.code) {
        case controls.PlayerOneAttack:
          secondFighter.health -= getDamage(firstFighter, secondFighter);
          secondFighterIndicator.style.width = `${(secondFighter.health * 100) / secondFighterInitHealth}%`;
          if (secondFighter.health <= 0) {
            secondFighterIndicator.style.width = '0%';
            document.removeEventListener('keypress', keypressHandler);
            resolve(firstFighter);
          }
          console.log(secondFighter.health);
          break;
        case controls.PlayerOneBlock:
          firstFighter.health -= getDamage(secondFighter, firstFighter);
          firstFighterIndicator.style.width = `${(firstFighter.health * 100) / firstFighterInitHealth}%`;
          if (firstFighter.health <= 0) {
            firstFighterIndicator.style.width = '0%';
            document.removeEventListener('keypress', keypressHandler);
            resolve(secondFighter);
          }
          break;
        case controls.PlayerTwoAttack:
          firstFighter.health -= getDamage(secondFighter, firstFighter);
          firstFighterIndicator.style.width = `${(firstFighter.health * 100) / firstFighterInitHealth}%`;
          if (firstFighter.health <= 0) {
            firstFighterIndicator.style.width = '0%';
            document.removeEventListener('keypress', keypressHandler);
            resolve(secondFighter);
          }
          break;
        case controls.PlayerTwoBlock:
          secondFighter.health -= getDamage(firstFighter, secondFighter);
          secondFighterIndicator.style.width = `${(secondFighter.health * 100) / secondFighterInitHealth}%`;
          if (secondFighter.health <= 0) {
            secondFighterIndicator.style.width = '0%';
            document.removeEventListener('keypress', keypressHandler);
            resolve(firstFighter);
          }
          break;
        case controls.PlayerOneCriticalHitCombination[0] &&
          controls.PlayerOneCriticalHitCombination[1] &&
          controls.PlayerOneCriticalHitCombination[2]:
          console.log('criticalAttack');
          const PlayerOneCriticalAttack = criticalAttackClosure(() => {
            secondFighter.health -= 2 * firstFighter.attack;
            secondFighterIndicator.style.width = `${(secondFighter.health * 100) / secondFighterInitHealth}%`;
          });
          if (PlayerOneCriticalAttack) PlayerOneCriticalAttack();
          console.log(firstFighter.health);
          if (firstFighter.health <= 0) {
            document.removeEventListener('keypress', keypressHandler);
            resolve(secondFighter);
          }
          break;
        case controls.PlayerTwoCriticalHitCombination[0] &&
          controls.PlayerTwoCriticalHitCombination[1] &&
          controls.PlayerTwoCriticalHitCombination[2]:
          console.log('criticalAttack');
          const PlayerTwoCriticalAttack = criticalAttackClosure(() => {
            firstFighter.health -= 2 * secondFighter.attack;
            firstFighterIndicator.style.width = `${(firstFighter.health * 100) / firstFighterInitHealth}%`;
          });
          if (PlayerTwoCriticalAttack) PlayerTwoCriticalAttack();
          console.log(secondFighter.health);
          if (secondFighter.health <= 0) {
            secondFighterIndicator.style.width = '0%';
            document.removeEventListener('keypress', keypressHandler);
            resolve(firstFighter);
          }
          break;
        default:
          break;
      }
    };
    document.addEventListener('keypress', keypressHandler);
  });
}

export function getDamage(attacker, defender) {
  // return damage

  const hitPower = attacker ?  getHitPower(attacker) : 0;
  const blockPower = defender ? getBlockPower(defender) : 0;

  return hitPower <= blockPower ? 0 : hitPower - blockPower;
}

export function getHitPower(fighter) {
  // return hit power

  const { attack } = fighter;
  const criticalHitChance = Math.floor(Math.random() * 2) + 1;
  const power = attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter) {
  // return block power

  const { defense } = fighter;
  const dodgeChance = Math.floor(Math.random() * 2) + 1;
  const power = defense * dodgeChance;
  return power;
}
