import { createElement } from "../../helpers/domHelper";
import { showModal } from "./modal";

export function showWinnerModal(fighter) {
  // call showModal function 

  const bodyElement = createElement({
    tagName: 'p'
  });

  bodyElement.innerText = 'GAME OVER!!!';

  const arg = {
    title: `Oh yeah! Fighter ${fighter.name} won! ${fighter.name} is the winner!`,
    bodyElement,
    onClose: function() {
      document.location.reload();
    }
  }
  showModal(arg);
}

