import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  if (fighter) {
    const { name, health, defense, attack } = fighter;

    const container = createElement({
      tagName: 'div',
      className: 'fighter-preview___container',
    });

    const textWrapper = createElement({
      tagName: 'div',
      className: 'fighter-preview___text-wrapper',
    });

    const imgElement = createFighterImage(fighter);
    imgElement.classList.add('fighter-preview___img');

    const nameElement = createElement({
      tagName: 'p',
      className: 'fighter-preview___name',
    });

    const healthElement = createElement({
      tagName: 'p',
      className: 'fighter-preview___text',
    });

    const defenseElement = createElement({
      tagName: 'p',
      className: 'fighter-preview___text',
    });

    const attackElement = createElement({
      tagName: 'p',
      className: 'fighter-preview___text',
    });

    nameElement.innerText = name;
    healthElement.innerText = `health: ${health}`;
    defenseElement.innerText = `defense: ${defense}`;
    attackElement.innerText = `attack: ${attack}`;

    fighterElement.append(container);
    positionClassName === 'fighter-preview___right'
      ? container.append(imgElement, textWrapper)
      : container.append(textWrapper, imgElement);
    textWrapper.append(nameElement, healthElement, defenseElement, attackElement);
  }
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
